import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes} from '@angular/router';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PasswordModule } from 'primeng/password';
// import { CookieService } from 'angular2-cookie/services/cookies.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AjoutUtilisateurComponent } from './echo/ajout-utilisateur/ajout-utilisateur.component';
import { ModifierUtilisateurComponent } from './echo/modifier-utilisateur/modifier-utilisateur.component';
import { CreerGroupeComponent } from './echo/creer-groupe/creer-groupe.component';
import { EnvoyerMessageComponent } from './echo/envoyer-message/envoyer-message.component';
import { ConnexionComponent } from './echo/connexion/connexion.component';
import { AjouterUtilisateurGroupeComponent } from './echo/ajouter-utilisateur-groupe/ajouter-utilisateur-groupe.component';


import { ConnexionService } from './services/connexion.service';
import { InscriptionService } from './services/inscription.service';
import { ModifierUtilisateurService } from './services/modifier-utilisateur.service';

@NgModule({
  declarations: [
    AppComponent,
    AjoutUtilisateurComponent,
    ModifierUtilisateurComponent,
    CreerGroupeComponent,
    EnvoyerMessageComponent,
    ConnexionComponent,
    AjouterUtilisateurGroupeComponent
   // UserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    PasswordModule,
    RouterModule.forRoot([
      {path: 'AjoutUtilisateur', component: AjoutUtilisateurComponent},
      {path: 'Connexion', component: ConnexionComponent},
      {path: 'ModifierUtilisateur', component: ModifierUtilisateurComponent},
      {path: 'CreerGroupe', component: CreerGroupeComponent},
      {path: 'EnvoyerMessage', component: EnvoyerMessageComponent},
      {path: 'AjouterUtilisateurGroupe', component: AjouterUtilisateurGroupeComponent}

    ])
  ],
  providers: [
    ConnexionService,
    InscriptionService,
    ModifierUtilisateurService,
    // CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
