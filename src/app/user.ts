import { Message } from './message';
import { Groupe } from './groupe';

export class User {
    Login: string;
    Mail: string;
    Mdp: string;
    ListGroupes?: Groupe[];
    ListMessages?: Message[];

    public User(login: string, mail: string, mdp: string) {
        this.Login = login;
        this.Mail = mail;
        this.Mdp = mdp;
    }
}
