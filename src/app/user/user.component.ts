import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MessageService } from 'primeng/api';
import { User } from '../user';
import { ConnexionService } from '../services/connexion.service';


/*@Component({
  selector: 'app-user',
  styleUrls: ['./user.component.css'],
  templateUrl: './user.component.html'
})*/

export class UserComponent implements OnInit {


  user: User;

  users: User[];

  selectedUser: User;
  selectedUserLogin: string;
  selectedUserMail: string;

  addUserForm: FormGroup;
  submitted = false;

  displayAddUser = false; // Affichage de la fenetre en overlay Ajout utilisateur
  displayUpdateUser = false; // Affichage de la fenetre en overlay mod utilisateur
  displayTable = false;

  // a checker
  @Output()
  processRemoveUser = new EventEmitter();

  constructor( private service: ConnexionService,
    private formBuilder: FormBuilder,
    private messageFenetreService: MessageService) { }

  ngOnInit() {
    // this.getUsers();
    // this.addUser();
  }

  // get f() { return this.addUserForm.controls; }


  // addUser() {
  //   this.addUserForm = this.formBuilder.group({
  //     login: ['', Validators.required],
  //     mail: ['', Validators.required],
  //     mdp: ['', Validators.required],
  //     mdpConfirm: ['', Validators.required]
  //   });
  // }

  // onSubmit() {
  //   this.submitted = true;
  //   if (this.addUserForm.invalid) {
  //   return;
  //   }
  //       // Récupération du login,mail et du mot de passe
  //   let formLogin = this.addUserForm.get('login').value;
  //   let formMail = this.addUserForm.get('mail').value;
  //   let formMdp = this.addUserForm.get('mdp').value;
  //   let formMdpConfirm = this.addUserForm.get('mdpConfirm').value;


  //   if (formMdpConfirm !== formMdp) {
  //     this.showMessage({severity: 'error', summary: 'Erreur', detail: 'Les mots de passe sont différents'});
  //   }

    // Verification si login non utilisé

    // this.service.connect(formLogin).subscribe(user => {
    //   if (user != null) {
    //     this.showMessage({severity: 'error', summary: 'Erreur', detail: 'Le login ou le mail est déjà utilisé'});
    //   } else {
    //   this.user = { Login: formLogin, Mail: formMail, Mdp: formMdp};
    //   this.service.addUser(this.user).subscribe(user => {this.users.push(user)});

    //   this.displayAddUser = false;

    //   this.showMessage({severity: 'success', summary: 'Message', detail: 'Ajout d\'un utilisateur réussi'});
    //   }
    // });
    // }

  // getUsers(): void {
  //   this.service.getUsers().subscribe(user => {
  //     this.users = user;
  //   });
  // }

  // Afficher le formulaire et remplir les champs

  // onSelected(user: User): void {
  //   this.displayAddUser = false;
  //   this.displayUpdateUser = true;
  //   this.selectedUser = user;
  //   this.addUserForm.get('login').setValue(user.Login);
  //   this.addUserForm.get('mail').setValue(user.Mail);
  //   this.addUserForm.get('mdp').setValue('');
  //   this.addUserForm.get('mdpConfirm').setValue('');

  }

  // delUser(user: User): void {
  //   this.users = this.users.filter(u => u !== user);
  //   this.service.delUser(user.Login).subscribe();
  //   this.showMessage({severity: 'success', summary: 'Message', detail: 'Suppression de l\'utilisateur réussie'});
  // }

  // updateUser() {
  //   const formLogin = this.addUserForm.get('login').value;
  //   const formMail = this.addUserForm.get('mail').value;
  //   const formMdp = this.addUserForm.get('mdp').value;
  //   const formMdpConfirm = this.addUserForm.get('mdpConfirm').value;

  //   if (formMdpConfirm !== formMdp) {
  //     this.showMessage({severity: 'error', summary: 'Erreur', detail: 'Les mots de passe sont différents'});
  //   } else {
  //     this.user = {Login: formLogin, Mail: formMail, Mdp: formMdp};
  //     this.service.updateUser(this.selectedUserLogin, this.user).subscribe();
  //     this.service.UpdateUser(this.selectedUserMail, this.user).subscribe();
  //     this.displayUpdateUser = false;

  //     this.showMessage({severity: 'success', summary: 'Message', detail: 'Modification de l\'utilisateur réussie'});
  //   }
  // }
  // showAddUser()
  // showAjoutUser() {
  //   this.displayUpdateUser = false;
  //   this.displayAddUser = true;
  //   this.addUserForm.get('login').setValue('');
  //   this.addUserForm.get('mail').setValue('');
  //   this.addUserForm.get('mdp').setValue('');
  //   this.addUserForm.get('mdpConfirm').setValue('');

  // }

  // showTable() {
  //   this.displayTable = true;
  // }
  // Fenetre de confirmation
  // showConfirmDel(user: User) {
  //   this.onHide();
  //   this.messageFenetreService.clear();
  //   this.user = user;
  //   this.messageFenetreService.add({key: 'del', sticky: true, severity: 'error',
  //   summary: 'Etes-vous sûr de vouloir supprimer cet utilisateur : ' + user.Login + ' ?', detail: 'Confirmer pour continuer'});
  // }
  // onConfirmDel() {
  //   this.delUser(this.user);
  //   this.messageFenetreService.clear('del');
  // }
  // onCancelDel() {
  //   this.messageFenetreService.clear('del');
  // }

  // clear() {
  //   this.messageFenetreService.clear();
  // }

  // // Fenetre des pop ups
  // showMessage(messageFenetreService) {
  //   this.clear();
  //   this.messageFenetreService.add(messageFenetreService);
  // }
  // onHide() {
  //   this.displayAddUser = false;
  //   this.displayUpdateUser = false;
  // }



//}
