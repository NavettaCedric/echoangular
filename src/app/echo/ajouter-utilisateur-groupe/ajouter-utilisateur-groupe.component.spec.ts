import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterUtilisateurGroupeComponent } from './ajouter-utilisateur-groupe.component';

describe('AjouterUtilisateurGroupeComponent', () => {
  let component: AjouterUtilisateurGroupeComponent;
  let fixture: ComponentFixture<AjouterUtilisateurGroupeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterUtilisateurGroupeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterUtilisateurGroupeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
