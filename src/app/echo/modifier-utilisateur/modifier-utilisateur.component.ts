import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {  ModifierUtilisateurService } from '../../services/modifier-utilisateur.service';
import { ConnexionService } from '../../services/connexion.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-modifier-utilisateur',
  templateUrl: './modifier-utilisateur.component.html',
  styleUrls: ['./modifier-utilisateur.component.css']
})
export class ModifierUtilisateurComponent implements OnInit {

  /*Email: string = 'email';
  Mdp: string = 'mdp';
  ConfirmMdp: string = 'confirm';

  @Input()
  email: string;
  @Input()
  mdp: string;
  @Input()
  confirmMdp: string;*/
@Input()
userMod:string;

utilisateurmodif = new FormGroup({
  Email: new FormControl(''),
  Password: new FormControl(''),
  PasswordConfirm : new FormControl('')
});

userModify:string;


  constructor(private service:  ModifierUtilisateurService,private router: Router) { }

  ngOnInit() {
  }
   
   onFormSubmit(){
    const password = this.utilisateurmodif.controls.Password.value;
    const passwordConfirm = this.utilisateurmodif.controls.PasswordConfirm.value;
   
    if ( password.value ==  passwordConfirm.value) {
      const userMod ={
        'Email' : this.utilisateurmodif.controls.Email.value,
        'Password' : this.utilisateurmodif.controls.Password.value
   };
  this.ModificationUser(userMod);
  }
}

  ModificationUser(utilisateurmodif: any){
    var fieldName1 ='Email';
     var fieldValue1 = utilisateurmodif['Email'];
     var fieldName2 ='Password';
     var fieldValue2 = utilisateurmodif['Password'];

     var obj1 ='{"' + fieldName1 + '":' ;
     obj1 = obj1 + '"' + utilisateurmodif['Email'] + '"}';
     var obj2 ='{"' + fieldName2 + '":' ;
     obj2 = obj2 + '"' + utilisateurmodif['Password'] + '"}';

  var obj3 =JSON.parse(obj1);
  var obj4 =JSON.parse(obj2);

  if(obj3['Email'] != "toto@gmail.com"){
    this.userMod= " Le mail  " + obj3['Email'] + "  Password :" + obj4['Password'] + " a bien été modifié";
  } else this.userMod ="Une erreur est survenue lors de la modification de ce mail !";

  }

}
