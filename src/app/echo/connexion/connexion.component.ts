import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ConnexionService } from '../../services/connexion.service';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  auth = new FormGroup({

    Name : new FormControl(''),
    Password : new FormControl('')
  });
//injection du message de connexion
  @Input()
  messageConnexion: string;
  donner: string;
  connectedUser: string;

  constructor(private service: ConnexionService, private router: Router, /*private cookie: CookieService*/) { }

  ngOnInit() {
    this.connectedUser = localStorage.getItem('user');

  }

  getAuth() {
    const donner = {
      'Name': this.auth.controls.Name.value,
      'Password': this.auth.controls.Password.value
    };

    this.Authentification(donner);
  }


  Authentification(donnerFormulaire: any) {

    var fieldName1 = 'Name';
    var fieldValue1 = donnerFormulaire['Name'];    
    var fieldName2 = 'Password';
    var fieldValue2 = donnerFormulaire['Password'];
    
    var obj1 = '{"' + fieldName1 + '":';
    obj1 = obj1 + '"' + donnerFormulaire['Name'] + '"}';
    var obj = JSON.parse(obj1);

   if(obj['Name'] != "toto"){
    this.messageConnexion = "Bienvenue, vous êtes connecté en tant que "+ obj['Name'];
   }
   else this.messageConnexion = " Pas de login correspondant!";


    //requête API
    // this.service.envoieConnexion(donnerFormulaire).subscribe(
    //   (data: any) => { /*this.router.navigate(['/ModifierUtilisateur']);*/
    //   localStorage.setItem('user', JSON.stringify(data));
    //   console.log(data); },
    //   error => { console.log(error); }
    // );
  }

}
