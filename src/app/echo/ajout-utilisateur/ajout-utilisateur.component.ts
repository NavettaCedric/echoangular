import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { InscriptionService } from '../../services/inscription.service';
import { ConnexionService } from '../../services/connexion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ajout-utilisateur',
  templateUrl: './ajout-utilisateur.component.html',
  styleUrls: ['./ajout-utilisateur.component.css']
})
export class AjoutUtilisateurComponent implements OnInit {

@Input()
userAdd:string;

utilisateur = new FormGroup({
  Name : new FormControl(''),
  Email : new FormControl(''),
  Password : new FormControl(''),
  PasswordConfirm : new FormControl('')
});

  connectedUser: string;


  constructor(private service: InscriptionService, /*private serviceConnexion: ConnexionService*/private router: Router) { }

  ngOnInit() {
    // this.connectedUser = JSON.parse(localStorage.getItem('user'));
    // this.serviceConnexion.envoieConnexion(this.connectedUser);
  }
    

 

  onFormSubmit() {
    const password = this.utilisateur.controls.Password.value;
    const passwordConfirm = this.utilisateur.controls.PasswordConfirm.value;

    if ( password.value ==  passwordConfirm.value) {
      const user ={
        'Name' : this.utilisateur.controls.Name.value,
        'Email': this.utilisateur.controls.Email.value,
        'Password' : this.utilisateur.controls.Password.value
     };

      this.Inscription(user);

    }
  }

  Inscription(utilisateur: any) {
    // this.service.requeteInscription(utilisateur).subscribe(
    //   (data: any) => { },
    //   (error) => {}
    // );
     var fieldName1 ='Name';
     var fieldValue1 = utilisateur['Name'];
     var fieldName2 ='Email';
     var fieldValue2 = utilisateur['Email'];
     var fieldName3 ='Password';
     var fieldValue3 = utilisateur['Password'];

     var obj1 ='{"' + fieldName1 + '":' ;
     obj1 = obj1 + '"' + utilisateur['Name'] + '"}';
     var obj2 ='{"' + fieldName2 + '":' ;
     obj2 = obj2 + '"' + utilisateur['Email'] + '"}';
     var obj3 ='{"' + fieldName3 + '":' ;
     obj3 = obj3 + '"' + utilisateur['Password'] + '"}';
     

     var obj =JSON.parse(obj1);
     var obj4 =JSON.parse(obj2);
     var obj5 =JSON.parse(obj3);

     if(obj['Name'] != "toto"){
       this.userAdd= " Le user " + obj['Name'] + " mail :" + obj4['Email'] + " password : "+  obj5['Password'] + " a bien été ajouté";
     } else this.userAdd ="Une erreur est survenue lors de l'ajout de ce user !";


  }
}
