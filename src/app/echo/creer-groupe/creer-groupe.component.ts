import { Component, OnInit, Input } from '@angular/core';
import { NgForm, FormControl, FormGroup, Form } from '@angular/forms';
import {CreerGroupeService} from '../../services/creer-groupe.service';
import { ConnexionService } from '../../services/connexion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-creer-groupe',
  templateUrl: './creer-groupe.component.html',
  styleUrls: ['./creer-groupe.component.css']
})
export class CreerGroupeComponent implements OnInit {
  
  @Input()
  groupMessage:string;
   
   groupeForm = new FormGroup({
    
    Name : new FormControl('')

  });
  donner:string;

  constructor(private service: CreerGroupeService,private router: Router) { }

  ngOnInit() {
  }

  onFormSubmit(){
    const donner = {
      'Name': this.groupeForm.controls.Name.value
    };

    this.CreateGroupe(donner);
  }

  setGroup() {
    // this.service.envoieCreerGroupe(this.creerGroupe);
    // const Donnee = {
    //   'Title': this.creerGroupe.controls.TitreGroupe.value
    // };
     
    // console.log('setGroup : ' + Donnee);
    // this.CreateGroupe(Donnee);
  }


  CreateGroupe(donneeFormulaire: any) {
    
    
    var fieldName1 = 'Title';
    var fieldValue1 = donneeFormulaire['Name'];    
    

    var obj1 = '{"' + fieldName1 + '":';
    obj1 = obj1 + '"' + donneeFormulaire['Name'] + '"}';
    var obj = JSON.parse(obj1);
    
    //console.log(object['Name']);
   if(obj['Title'] != "toto"){
    this.groupMessage = "Le groupe  "+ obj['Title'] +" a bien été créé";
   }
   else this.groupMessage = " Une erreur est survenue lors de la création du groupe!";


    //this.groupName =donneeFormulaire.Title.value;
   
  //   console.log('Creategroupe : ' + donneeFormulaire);
  //   this.service.envoieCreerGroupe(donneeFormulaire).subscribe(
  //     (data: any) => { console.log(data); },
  //     error => { console.log(error); }
  //   );
 }
}

