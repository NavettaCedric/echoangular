import { TestBed } from '@angular/core/testing';

import { ModifierUtilisateurService } from './modifier-utilisateur.service';

describe('ModifierUtilisateurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ModifierUtilisateurService = TestBed.get(ModifierUtilisateurService);
    expect(service).toBeTruthy();
  });
});
