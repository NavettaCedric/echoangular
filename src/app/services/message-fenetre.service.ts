import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageFenetreService {

  messagesFenetre: string[] = [];

  constructor() { }

  add(messageFenetre: string) {
    this.messagesFenetre.push(messageFenetre);
  }

  clear() {
    this.messagesFenetre = [];
  }


}
