import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CreerGroupeService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })

  };

  envoieCreerGroupe(donneeFormulaire: any): any {
    console.log('envoieCreeGroupe : ' + donneeFormulaire);
    const body = JSON.stringify(donneeFormulaire);
    console.log('envoieCreeGroupe : ' + body);
    return this.http.post('https://echorestapi3.azurewebsites.net/api/Groups/', body, this.httpOptions);
  }
}
