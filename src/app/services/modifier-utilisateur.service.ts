import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ModifierUtilisateurService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  envoieModification(donneeFormulaire: any): any {
    const body = JSON.stringify(donneeFormulaire);
    return this.http.post('https://echorestapi3.azurewebsites.net/api/Users/1', body, this.httpOptions);
  }
}
