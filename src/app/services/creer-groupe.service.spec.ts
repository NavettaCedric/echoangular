import { TestBed } from '@angular/core/testing';

import { CreerGroupeService } from './creer-groupe.service';

describe('CreerGroupeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CreerGroupeService = TestBed.get(CreerGroupeService);
    expect(service).toBeTruthy();
  });
});
