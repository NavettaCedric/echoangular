import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { stringify } from '@angular/core/src/render3/util';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  constructor(private http: HttpClient) { }

  httpOptions = {
    withCredentials: true,
    headers : new HttpHeaders({
      'Content-Type': 'apllication/json'
    })
  };


  requeteInscription(utilisateur: any) {

    const body = JSON.stringify(utilisateur);
    return this.http.post('https://echorestapi3.azurewebsites.net/api/Users/', body, this.httpOptions);

  }

  
}
