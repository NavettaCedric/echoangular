import { TestBed } from '@angular/core/testing';

import { MessageFenetreService } from './message-fenetre.service';

describe('MessageFenetreService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessageFenetreService = TestBed.get(MessageFenetreService);
    expect(service).toBeTruthy();
  });
});
