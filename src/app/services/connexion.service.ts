import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Form, NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})



export class ConnexionService {

  private  loggedInStatus = JSON.parse( localStorage.getItem('loggedin') || 'false');

  constructor(private http: HttpClient) { }


   httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  setLoggedIn(value: any) {
  }

  get isLoggedIn() {
    return JSON.parse(localStorage.getItem('loggedin') || this.loggedInStatus.toString());
  }

  envoieConnexion(donneeFormulaire: any): any {

    const body = JSON.stringify(donneeFormulaire);
    this.http.post('https://echorestapi3.azurewebsites.net/api/Auth', body, this.httpOptions);
  
  }

  getConnexion(user: any) {
    this.http.get('https://echorestapi3.azurewebsites.net/api/Auth', user);
  }
}
